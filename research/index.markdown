---
layout: page
title: Research
permalink: /research/
---

Currently, my research interests include but are not limited to

* ill-posed linear inverse problems in statistical estimation
* instrumental estimation in endogenous high-dimensional regressions
* dimension reduction techniques for large datasets
* seasonal economic time series short-term forecasting
