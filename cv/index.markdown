### Rytis Bagdziunas

* [rytis@rbagd.eu](mailto:rytis@rbagd.eu)
* Tel.: 0(032)/484.247.866
* XMPP: [rytis@jabber.belnet.be](xmpp:rytis@jabber.belnet.be)

<hr>

#### Profile

I am a statistical consultant with a strong background in mathematics and statistical programming. I am versatile with my approach, passionate about my profession and community, and enthusiastic about tackling the great data science problems of our age.

<hr>

#### Work experience

* *2016 Feb - present*. Statistical consultant at [Open Analytics NV](https://www.openanalytics.eu). Currently, I am mostly involved in a multitude of health-related statistical projects for the pharmaceutical and healthcare industry.
* *2012 - 2016*. Data analyst at [Service d'Analyse Economique](https://www.uclouvain.be/en-283724.html). My primary responsibility within the team was analysing and forecasting price inflation in Belgium as well as overview of emerging economies. Reports to which I have contributed can be accessed [here][regards].
* *2009 - 2016*. Teaching assistant for various university courses. I worked full time during academic year 2011-2012. On the whole, I assisted for microeonomics, (applied) econometrics as well as advanced econometrics courses.
* *2005 - 2008*. Summer jobs at the Bank of New York Mellon in Brussels

<hr>

#### Education

* *2011 - 2017*. **Ph.D. in econometrics**, CORE, Université catholique de Louvain
    * Title: *Estimation of high dimensional models with risk constraints*.
    * Supervisor: Sébastien Van Bellegem
    * Project details: We analyze the issue of endogeneity in the context of high-dimensional data, notably where data exhibits functional smoothness. In particular, we consider functional linear regression models and various regularization techniques for their estimation such as dimension reduction with PCA and Galerkin methods as well as L<sup>2</sup>-regularization.
    * Grant: project was funded by *Aspirant F.N.R.S.* scholarship from 2012 to 2016.
* *2009 - 2011*. **Master in Mathematics** (*magna cum laude*), Université catholique de Louvain
* *2008 - 2010*. **Research Master in Econometrics** (*magna cum laude*), Université catholique de Louvain

<hr>

#### Programming languages and technical profficiency

* R,
* SQL, notably `SQLite` and `PostgreSQL`
* Python
* JavaScript, especially when integrated with R
* Git
* Linux and shell scripting

I am familiar with `X13-ARIMA-SEATS` statistical adjustment packages as well as modern data exchange protocols, notably `SDMX-ML`. I am also familiar with `Apache Spark` basics, notably via `PySpark`, having followed [introductory][cert1] and [scalable machine learning][cert2] courses on *edX*.

<hr>

#### Publications

* Articles in progress:
    * *Estimation of functional instrumental linear regression with Galerkin methods*.
    * *Statistical inference on instrumental functional regression by Tikhonov regularization*.
* *`R` packages*:
    * `bigl` package to assess and visualize synergistic effects in a two-drug study. To be presented in useR!2017. 
    * `dynfactoR` package which facilitates estimation of state-space based dynamic factor models. Faster `Rcpp` based version which extends the model to allow for Kim filtering is currently in development.
    * `functionalReg` package implementing various estimators for functional linear regression models. This package is part of my Ph.D. program.

#### Human languages

* Lithuanian (mother tongue)
* French (fluent)
* English (fluent)
* German (basic)
* Russian (basic)

[regards]: http://www.regards-economiques.be/auteurs?cid=118
[cert1]: https://s3.amazonaws.com/verify.edx.org/downloads/04f53fb2d8cf48f9876f0ed466b30912/Certificate.pdf
[cert2]: https://s3.amazonaws.com/verify.edx.org/downloads/f582e2461bb345fabea93e46d5c4a727/Certificate.pdf
