---
layout: page
title: Projects
permalink: /projects/
---

Here is a list of publicly available coding projects I have written. You can find more details and source files on my [GitHub](https://github.com/rbagd) and [Bitbucket](https://bitbucket.org/rbagd) accounts.

* [Tool](http://spark.rstudio.com/rytis/inflation) for visualizing price inflation in Belgium. Written in `R` with `shiny` and `lattice`. I still keep it up-to-date, though due to change in statistical methodology since 2014, stacked bar charts can only be thought of as an approximation.
* [Automating treatment of certain variables](https://github.com/rbagd/fertility-us) within detailed US birth data. This is part of one of my research projects. It uses `shell` programming, `SQLite`, `python` and `ggplot` package in `R`.
* This website. It is powered by `Jekyll` and written in `markdown`. Its source code is [here](https://bitbucket.org/rbagd/perso-uclouvain).
