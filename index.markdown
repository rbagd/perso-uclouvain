---
layout: page
permalink: /
---

I am a Ph.D. student in econometrics at the Université catholique de Louvain, Belgium, holding a scholarship of [Aspirant FNRS](http://www.fnrs.be/).

#### Contacts

* [rytis.bagdziunas@uclouvain.be](mailto:rytis.bagdziunas@uclouvain.be "Email me")
* Mailing address:

> Voie du Roman Pays, 34 <br>
> L1.03.01 ([CORE](http://www.uclouvain.be/core)) <br>
> 1348 Louvain-la-Neuve, Belgium <br>
> Office no.: CORE b.339

* [rytis@jabber.belnet.be](xmpp:rytis@jabber.belnet.be "Contact me on Jabber/XMPP network")

I also keep a [personal non-academic weblog](http://rbagd.eu).

My PGP key ID is `0x4E0D7D37`. You can find my public key [here]( {{ site.baseurl }}/pgp.txt).
